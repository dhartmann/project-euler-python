import time
from utilities.prime import primesfrom3to

__author__ = 'David'

start_time = time.time()
max_prime = 10000000
max_n = 0
max_ab = 0
primes1000 = primesfrom3to(1000)
# TODO: should just calculate sieve directly since this uses sieve to get the primes and then back to sieve
primes = primesfrom3to(max_prime)
prime_cache = [False for i in range(max_prime)]
for prime in primes:
    prime_cache[prime] = True

prep_time = time.time()

for a in range(-999, 1001):
    for b in primes1000:
        n = 0
        prime = n * n + a * n + b
        while prime_cache[prime]:
            n += 1
            prime = n * n + a * n + b
        if n - 1 > max_n:
            max_n = n - 1
            max_ab = a * b
            print max_n, max_ab

end_time = time.time()
print 'Found {} consecutive primes for a*b = {} in {} seconds ({} prep time).'.format(max_n, max_ab, end_time - start_time, prep_time - start_time)

