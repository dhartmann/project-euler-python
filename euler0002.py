from utilities.fibonacci import fibonacci_range

print sum([i if i % 2 == 0 else 0 for i in fibonacci_range(4000000)])
