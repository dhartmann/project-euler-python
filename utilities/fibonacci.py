
def fibonacci_range(n):
    fibs = [0, 1]
    fib = fibs[len(fibs)-1] + fibs[len(fibs)-2]

    while fib < n:
        fibs.append(fib)
        fib = fibs[len(fibs)-1] + fibs[len(fibs)-2]

    return fibs